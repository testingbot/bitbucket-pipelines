TestingBot - Bitbucket Pipelines
====================================

This repository contains an example demonstrating the execution of Selenium tests on [TestingBot] with Bitbucket Pipelines.

* A simple web server (Sinatra) is started on port 8001 to serve two pages at `/` and `/welcome`
* To enable remote browsers and devices on TestingBot to access the local server [TestingBot Tunnel] is downloaded and used to establish a secure connection to TestingBot
* An RSpec test defined in `spec/welcome.rb` and it runs Selenium tests on [TestingBot]


## Usage

* Clone this repository or create a fork:
  - `git clone https://bitbucket.org/testingbot/bitbucket-pipelines.git`

* Add the following environment variables to your Bitbucket repository:
  - `TB_KEY`: (Required) TestingBot key
  - `TB_SECRET`: (Required) TestingBot secret

  Note: You can get these values from TestingBot [Account Settings].


## Build Configuration

```
image: ruby:2.1
pipelines:
  default:
    - step:
        script:
          - apt-get update
          - apt-get install unzip
          - bundle install
          - bundle exec rackup -p 8001 > /dev/null &
          - ./run_local.bash
          - bundle exec rspec spec/welcome.rb
          - pkill -9 rackup
```

* Sets the Docker image to be used for builds to Ruby 2.1
* Installs unzip utility for extracting `.zip` files
* Installs Ruby gems required by this example
* Starts Sinatra web server on port 8001
* Runs a bash script that downloads, extracts, and installs [TestingBot Tunnel]
* Runs the Selenium test cases defined in `spec/welcome.rb`
* Terminates the web server

[TestingBot Tunnel]:https://testingbot.com/support/other/tunnel
[TestingBot]:https://testingbot.com
[Account Settings]:https://testingbot.com/members/user/edit

