#!/bin/bash -e

wget https://testingbot.com/tunnel/testingbot-tunnel.jar
java -jar testingbot-tunnel.jar ${TB_KEY} ${TB_SECRET} > /dev/null &
sleep 10