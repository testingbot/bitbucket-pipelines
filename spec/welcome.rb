require 'selenium-webdriver'
require 'rspec'

RSpec.configure do |config| config.color_enabled = true end

describe "The welcome demo" do

  before(:all) do
    testingbot_key = ENV['TB_KEY']
    testingbot_secret = ENV['TB_SECRET']
    caps = Selenium::WebDriver::Remote::Capabilities.new
    caps['platform'] = 'WINDOWS'
    caps['version'] = 'latest'
    caps['browserName'] = 'chrome'
    caps['build'] = 'Bitbucket Pipeline'
    caps['name'] =  'Bitbucket Pipeline'
    @driver = Selenium::WebDriver.for(:remote, :url => "http://#{testingbot_key}:#{testingbot_secret}@hub.testingbot.com/wd/hub", :desired_capabilities => caps)
 end

  after(:all) do
    @driver.quit
  end

  it "should have a welcome page" do
    username = 'Test User'
    @driver.get 'http://localhost:8001'
    @driver.find_element(:name ,'name').send_keys username
    @driver.find_element(:css, "input[type='submit']").click
    actual_text = @driver.find_element(:css, "h2").text
    actual_text.should == "Welcome, #{username}"
  end

end